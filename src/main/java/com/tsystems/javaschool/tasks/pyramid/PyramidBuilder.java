package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int rows;
        int cols;
        int[][] result;
        if((inputNumbers.isEmpty())){
            throw new CannotBuildPyramidException();
        }
        for (Integer inputNumber : inputNumbers) {
            if (inputNumber == null) {
                throw new CannotBuildPyramidException();
            }
        }
        for (int i=0; i<inputNumbers.size(); i++){
            int count = 0;
            for (int j=0; j<inputNumbers.size(); j++){
                if (inputNumbers.get(i).equals(inputNumbers.get(j))){
                    count++;
                }
                if (count>1) {
                    throw new CannotBuildPyramidException();
                }
            }
        }
        Collections.sort(inputNumbers);

        int count = 0;
        rows = 1;
        cols = 1;
        while (count < inputNumbers.size()){ // counting number of cols and rows
            count = count + rows;
            rows++;
            cols +=2;
        }
        rows-=1; //actual values
        cols-=2;
        if (count != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        result = new int[rows][cols]; //result matrix with specified dimensions
        for (int[] row : result) { //filling matrix with 0
            Arrays.fill(row, 0);
        }

        int center = (cols / 2);//
        count = 1; //
        int arrIdx = 0; //

        for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
            int start = center - offset;
            for (int j = 0; j < count * 2; j +=2, arrIdx++) {
                result[i][start + j] = inputNumbers.get(arrIdx);
            }
        }

        return result;
    }


}
