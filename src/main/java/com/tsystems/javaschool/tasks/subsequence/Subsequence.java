package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if ((y == null)||(x == null)) {
            throw new IllegalArgumentException();
        } else if((y.size()< x.size())){
            return false;
        } else if ((x.size() == 0)||(y.size() == 0)) {
            return true;
        } else if ((x.size() == 0)&&(y.size() != 0)) {
            return true;
        }
        int i = 0; //pointer for x
        for (int j = 0; j < y.size(); j++){
            if(x.get(i).equals(y.get(j))) {
                i++;
                if(i > x.size()-1) {
                    break;
                }
            }
        }
        if (i<x.size() - 1) return false;
        return true;
    }

//    public static void main(String[] args) {
////        List<String> x = Arrays.asList(new String[]{"A", "B", "C", "E", "D"});
////        List<String> y = Arrays.asList(new String[]{"RR", "A", "H", "B", "E", "C","BB", "E", "D"});
//        List x = Stream.of(1, 3, 5, 7, 9).collect(toList());
//        List y = Stream.of(10, 1, 2, 3, 4, 3, 5, 7, 9, 20).collect(toList());
//
//        Subsequence ss = new Subsequence();
//
//        System.out.println(ss.find(x,y));
//    }
}
