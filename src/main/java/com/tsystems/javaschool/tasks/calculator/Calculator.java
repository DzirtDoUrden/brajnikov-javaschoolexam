package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {
    private static String operators = "+-*/";
    private static String delimiters = "()" + operators;



    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) { //this method uses reverse Polish notation

        if ((statement == null) || (statement.isEmpty())) return null;
        statement = statement.replaceAll(" ",""); //remove all spaces

        for (int i = 0; i < statement.length() - 1; i++) { //check two operators near
            String s1 = Character.toString(statement.charAt(i));
            String s2 = Character.toString(statement.charAt(i+1));
            if(isOperator(s1) && isOperator(s2)) {
                return null;
            }

        }
        /*
         * parsing all symbols to postfix form (reverse Polish notation)
         * result - postfixList
         */
        List<String> postfixList = new LinkedList<>();
        Deque<String> operationsStack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(statement, delimiters, true);

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(token)) {
                return null;
            }
            if (isDelimiter(token)) {
                if (token.equals("(")) {
                    operationsStack.push(token);
                } else if (token.equals(")")) {
                    while (!operationsStack.peek().equals("(")) {
                        postfixList.add(operationsStack.pop());
                    }
                    if (operationsStack.isEmpty()) {
                        return null;
                    }
                    operationsStack.pop();
                } else if(isOperator(token)) {
                    while (!operationsStack.isEmpty() && (getPriority(token) <= getPriority(operationsStack.peek()))) {
                        postfixList.add(operationsStack.pop());
                    }
                    operationsStack.push(token);
                }

            } else {
                try {
                    Double dd = Double.parseDouble(token);
                } catch (IllegalArgumentException e){
                    return null;
                }
                postfixList.add(token);
            }
        }
        while (!operationsStack.isEmpty()) {
            if (isOperator(operationsStack.peek())) {
                postfixList.add(operationsStack.pop());
            } else {
                return null;
            }
        }

        /*
        * end of parsing to reverse Polish notation
        *
        *
        * start counting
        */

        Deque<Double> stack = new ArrayDeque<>();
        for (String x : postfixList) {
            if (x.equals("+")){
                stack.push(stack.pop() + stack.pop());
            }
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            }
            else if (x.equals("*")) {
                stack.push(stack.pop() * stack.pop());
            }
            else if (x.equals("/")) {
                Double b = stack.pop();
                if (b == 0) {
                    return null;
                }
                Double a = stack.pop();
                stack.push(a / b);
            }
            else {
                stack.push(Double.valueOf(x));
            }
        }

        //if value with no decimals (like 5.0) - make it integer (5) and place into result string
        String result = stack.pop().toString();
        double x = Double.valueOf(result);
        if (x == Math.round(x)) {
            long xInt = Math.round(x);
            result = String.valueOf(xInt);
        }
        return result;
    }

    /**
     * checking delimiters
     * @param token String
     * @return true, if token is delimiter
     */
    private static boolean isDelimiter(String token){
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }
    /**
     * checking operators
     * @param token String
     * @return true, if token is operator
     */
    private static boolean isOperator(String token) {
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    /**
     * returns priority of token
     * @param token String
     * @return priority of token
     */
    private static int getPriority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+")||token.equals("-")) return 2;
        if (token.equals("*")||token.equals("/")) return 3;
        return 4;
    }

}
